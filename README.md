**[W.I.P]**

# Ateliers "sciences et techniques" école St-Ignace

École St Ignace (64270 CARESSE), Délphine DURCOS, Alex HAMON (pour [La Fab'Brique](http://lafabbrique.org))

Licence: [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) (Domaine Public)


Animation en classe multi-niveaux (CE/CM, 30 élèves) d'ateliers de découverte des sciences et techniques, débuté l'an passé (2016-2017) par des ateliers "mécaniques" dans le cadre d'un "Congrès des jeunes chercheurs" (finalement annulé...).

Cette nouvelle saison est dédiée aux sciences informatiques. La majorité des activités s'effectuent "débranchées", sans ordinateur. Le TBI et la classe mobile (9 postes) viennent conforter les notions vues durant les TP, et développer l'usage de l'outil informatique comme périphérique de création.

## Support préparatoire
[Voir le PDF](./ateliers_tech_St-Ignace.pdf)

## Contenu
  - Architecture, matériel (manipulation des composants)
  - Langage binaire (jeux de messages secrets)
  - Affichage d'images matricielles (dessin pixels)
  - Robotique (déplacements séquentiels)
  - Algorithmique (variables, fonctions, boucles)
  - Projet (TP Scratch)

## Projet
Création d'un jeu vidéo de type "Attrape l'objet qui tombe".

## Liens

### Ressources
  - [L’informatique sans ordinateur](https://interstices.info/upload/docs/application/pdf/2014-06/csunplugged2014-fr.pdf) (CSunplugged - pdf Fr)
  - [Activités débranchées à Grenoble](http://www-irem.ujf-grenoble.fr/spip/spip.php?article146)
  - [Activités débranchées à Clermont-Ferrand](http://people.irisa.fr/Martin.Quinson/Mediation/SMN/)
  - [IREM Clermont-Ferrand](http://www.irem.univ-bpclermont.fr/Informatique-sans-Ordinateur)
  - [La science informatique dans les programmes scolaires](http://tabs.chalifour.fr/la-science-informatique-a-lecole/la-science-informatique-dans-les-programmes/)
  - [TP et cartes "Programmer des déplacements" (monecole.fr)](http://monecole.fr/wp-content/uploads/2017/02/sequence-programmation.pdf)
  - [Définitions (ressources91.ac-versailles)](http://www.ressources91.ac-versailles.fr/wordpress/algorithme-code-et-robotique-dans-les-programmes/)
  - [TP initiation Scratch (ressources91.ac-versailles)](http://www.ressources91.ac-versailles.fr/wordpress/utiliser-scratch-en-classe/)
  - [Tutos Scratch en Fr](http://squeaki.recitmst.qc.ca/ProjetsScracth)
  - [Brickodeurs.fr](https://brickodeurs.fr/)
  - 

### Logiciels
  - [Scratch](https://scratch.mit.edu/) (Langage de programmation par blocs - Multiplateforme + En ligne -_Nécessite Flash!_-)
  - [GeoTortue](http://geotortue.free.fr) (Logiciel pour apprendre la géométrie, basé sur le langage LOGO. Redondance avec Scratch + "crayon"?)
  - [Blockly](https://developers.google.com/blockly/) (Langage de programmation par blocs - En ligne)
    - [Instance Blockly Fr](http://blockly.fr/blockly-master/demos/code/index.html?lang=fr)
    - [Jeux Blockly](https://blockly-games.appspot.com/?lang=fr)
  - [ScratchJR](https://www.scratchjr.org/) (Instructions en ligne de symboles - Android/Ios)
  - [LightBot](http://lightbot.com/) (Instructions en ligne de symboles, devient vite complexe avec l'ajout de boucles, conditions... - Android/Ios)
  - [Tuxbot](http://appli-etna.ac-nantes.fr:8080/ia53/tice/ressources/tuxbot/index.php) (Instructions en ligne de symboles, Les tableaux sont téléchargeables sous forme de livret à imprimer - Android/Windows)
  - [code.org](https://code.org/learn) (Cours et jeux de programmation - En ligne)
  - [SuperPowers](http://superpowers-html5.com/index.fr.html) (logiciel de création de jeux vidéos 2D, langage TypeScript)
  - [Liens sur Brickodeurs.fr](https://brickodeurs.fr/apprendre-a-programmer-en-programmant-des-jeux/)
  - 

## Scéances

### 16/03
  - Introduction/présentation
  - Architecture (démontage PC et reconnaissance composants)
 

### 30/03
 - Architecture suite
 - JdR PC
 

### 07/05
...
